from django.urls import path,include
from django.conf.urls import url

from . import views
from django.contrib.auth import views as auth_views
app_name = 'demo'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    path('form', views.form_view, name='form_view'),

]