from django.db.models.signals import post_save, pre_delete, pre_save
from django.dispatch import receiver
from .models import Demo

@receiver(post_save, sender=Demo)
def demo_credit_score_calculation(sender, instance, created, **kwargs):
    #get the data that has been submitted
    if created:
        new_recharge_amount = instance.Recharge_amount
        print(new_recharge_amount)
