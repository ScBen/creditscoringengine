from django.shortcuts import render, redirect
from .forms import DemoForm
from django.contrib.auth.decorators import login_required

 # Create your views here.


@login_required
def index(request):
    return render(request, 'index.html')

# def form_view(request):
    # return render(request,'form_view.html')


def form_view(request):
	if request.method == "POST":
		form = DemoForm(request.POST)
		if form.is_valid():
			data = form.save(commit=False)
			data.save()
			return render(request, 'form_view.html')
	else:
		form = DemoForm()
	return render(request, 'form_view.html', {'form' : form})
    #return render(request,'form_view.html',{'form' : form})

