from django.db import models

# Create your models here.
class Demo(models.Model):
    #Index = models.CharField(max_length=250, default='none', db_column='index')
    Recharge_amount = models.IntegerField(db_column='Recharge Amount')
    Airtime_Balance = models.IntegerField(db_column='Airtime Balance')
    Recharge_date = models.DateField(db_column='Recharge Date')
    Mpesa_Transaction = models.IntegerField(db_column='Mpesa Transaction')
    Money_In_Out = models.BooleanField(db_column='Flag')
    #Mpesa_Transaction_Cost = models.IntegerField(db_column='Mpesa Transaction Cost')
    Mpesa_Balance = models.IntegerField(db_column='Mpesa Balance')
    #Line_Activation_Date = models.DateTimeField(db_column='Line Activation Date')
    #Line_Phone_Number = models.IntegerField(blank=True, null=True, db_column='Line Phone Number')
    Call_Type = models.BooleanField(db_column='Call Type', max_length=250)
    Call_Duration = models.CharField(db_column='Call Duration', max_length=250)
    Call_Date = models.DateField(db_column='Call Date')
    Device_Brand = models.CharField(db_column='Device Brand', max_length=250)
    #Sim_State = models.CharField(db_column='Sim State', max_length=250)