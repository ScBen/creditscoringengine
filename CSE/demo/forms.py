from django import  forms
from . models import Demo


class DemoForm(forms.ModelForm):

    class Meta:
        model = Demo
        fields = ('Recharge_amount','Airtime_Balance','Recharge_date','Mpesa_Transaction','Money_In_Out','Mpesa_Balance'
                  ,'Call_Type','Call_Duration','Call_Date','Device_Brand',)