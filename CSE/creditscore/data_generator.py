import numpy
import pandas as pd
from numpy.random import randint
airtime_balance_values = numpy.around(numpy.random.uniform(low=0.0, high=150.0, size=(500,)),2)
recharge_values = randint(5, 80, 500)
mpesa_in_out = [1] *500
call_type_in = [0] * 200
call_type_out = [1] * 300
device_brand_1 = ["Huawei"] * 200
device_brand_2 = ["Samsung"] * 100
device_brand_3 = ["Iphone"] * 50
device_brand_4 = ["Tecno"] * 150
device_brands = device_brand_1 + device_brand_2 + device_brand_3 + device_brand_4
target_1 = ["yes"] * 50
target_2 = ["no"] * 50
target = target_1 + target_2 + target_1 + target_1 + target_2 + target_2 + target_2 + target_1 + target_2 + target_2

call_type_values = call_type_in + call_type_out
call_duration_in_secs = randint(5,3600,500)
mpesa_transaction = randint(200,10000,500)

mpesa_transaction_values = numpy.around(mpesa_transaction/100, decimals=0)*100
airtime_recharge_values = numpy.around(recharge_values/5, decimals=0)*5
mpesa_balance = randint(0,9000,500)


start = pd.to_datetime('2020-04-01')
end = pd.to_datetime('2020-06-01')
n = 500

start_u = start.value // 10 ** 9
end_u = end.value // 10 ** 9
dates = pd.DatetimeIndex((10 ** 9 * numpy.random.randint(start_u, end_u, n, dtype=numpy.int64)).view('M8[ns]'))
list_of_dates = dates.to_list()

list_of_values = airtime_balance_values.tolist()
list_of_recharge = airtime_recharge_values.tolist()
list_of_mpesa_transaction = mpesa_transaction_values.tolist()
list_of_mpesa_balance = mpesa_balance.tolist()
list_of_call_duration = call_duration_in_secs.tolist()

df = pd.DataFrame(data={"airtime_balance": list_of_values, "airtime_recharge": list_of_recharge, "recharge_date": list_of_dates,"mpesa_transaction": mpesa_transaction_values,
                       "mpesa_in_out": mpesa_in_out,"mpesa_balance":list_of_mpesa_balance, "call_type": call_type_values, "call_duration_in_secs" : list_of_call_duration
                       ,"call_date": list_of_dates, "device_brands": device_brands,"target":target})
df.to_csv("./airtime.csv", sep=',',index=False)