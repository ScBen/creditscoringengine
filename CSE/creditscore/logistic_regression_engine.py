import pandas as pd
#import packages
import os
import numpy as np
import pandas as pd
import pandas.core.algorithms as algos
from pandas import Series
import scipy.stats.stats as stats
import re
import traceback
import string
#from pymongo import MongoClient
from sklearn import preprocessing
import joblib
import scorecardpy as sc

from  weightIv import ready_data
if __name__ == '__main__':
    le = preprocessing.LabelEncoder()
    encoded_data = ready_data.apply(le.fit_transform)

    # breaking dt into train and test
    train, test = sc.split_df(encoded_data, 'target').values()


    bins_adj = sc.woebin(encoded_data, y="target")



    # converting train and test into woe values
    train_woe = sc.woebin_ply(train, bins_adj)
    test_woe = sc.woebin_ply(test, bins_adj)

    y_train = train_woe.loc[:, 'target']
    X_train = train_woe.loc[:, train_woe.columns != 'target']
    y_test = test_woe.loc[:, 'target']
    X_test = test_woe.loc[:, train_woe.columns != 'target']

    # import the class
    from sklearn.linear_model import LogisticRegression

    # instantiate the model (using the default parameters)
    logreg = LogisticRegression()


    # fit the model with data
    logreg.fit(X_train,y_train)


    #save the model to disk
    filename = 'trained_lr_model.sav'
    joblib.dump(logreg, filename)

    #load the trained model
    trained_model = joblib.load(filename)

    y_pred=trained_model.predict(X_test)

    # import the metrics class
    from sklearn import metrics
    cnf_matrix = metrics.confusion_matrix(y_test, y_pred)

    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
    print("Precision:",metrics.precision_score(y_test, y_pred))
    print("Recall:",metrics.recall_score(y_test, y_pred))


    #score development
    score = sc.scorecard(bins_adj, logreg, X_train.columns)

    #credit score
    train_score = sc.scorecard_ply(train, score, print_step=0)
    test_score = sc.scorecard_ply(test, score, print_step=0)
    print(test_score)


