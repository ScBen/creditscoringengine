from django.db import models
from django.utils import timezone
#from phonenumber_field.modelfields import PhoneNumberField


class Applicant_detail(models.Model):
    Index = models.CharField(max_length=250, default='none', db_column='index')
    Full_Name = models.CharField(max_length=250, db_column='Full Name')
    Secondary_Name = models.CharField(max_length=250, blank=True, null=True,db_column='Secondary Name')
    ID_Number = models.IntegerField(db_column='ID Number')
    Gender = models.CharField(max_length=250,db_column='Gender')
    Age = models.IntegerField(db_column='Age')
    Education_Level = models.CharField(max_length=250,db_column='Education Level')
    Profession = models.CharField(max_length=250,db_column='Profession')
    Occupation = models.CharField(max_length=250,db_column='Occupation')
    Religion = models.CharField(max_length=250,db_column='Religion')
    Income_Level = models.IntegerField(db_column='Income Level')
    Marital_Status = models.CharField(max_length=250,db_column='Marital Status')
    Nationality = models.CharField(max_length=250,db_column='Nationality')
    Email_Address = models.EmailField(blank=True, null=True,db_column='Email Address')
    Created_Time = models.DateTimeField(default=timezone.now, db_column='Created Time')
    Updated_Time = models.DateTimeField(default=timezone.now, db_column='Updated Time')


    def get_formatted_datetime(self):
        return self.created_time.strftime('%d-%m-%Y %H:%M')

class Loan_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='loan_details',null=True, on_delete=models.CASCADE)
    Loan_Amount = models.IntegerField(db_column='Loan Amount')
    Loan_Time_Length = models.IntegerField(db_column='Loan Time Length')
    Reason_For_Loan = models.CharField(max_length=250, db_column='Reason For Loan')

    class Meta:
        verbose_name_plural = "loan_details"


class Airtime_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='airtime_details',null=True, on_delete=models.CASCADE)
    Recharge_amount = models.IntegerField(db_column='Recharge Amount')
    Airtime_Balance = models.IntegerField(db_column='Airtime Balance')
    Recharge_date = models.DateTimeField(auto_now_add=True,db_column='Recharge Date')

    class Meta:
        verbose_name_plural = "airtime_details"

class Mpesa_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='mpesa_details',null=True, on_delete=models.CASCADE)
    Mpesa_Transaction = models.IntegerField(db_column='Mpesa Transaction')
    Money_In_Out = models.BooleanField(db_column='Flag')
    Mpesa_Reference_Number = models.CharField(db_column='Mpesa Reference Number', max_length=250)
    Mpesa_Transaction_Cost = models.IntegerField(db_column='Mpesa Transaction Cost')
    Mpesa_Balance = models.IntegerField(db_column='Mpesa Balance')

    class Meta:
        verbose_name_plural = "mpesa_details"

class Line_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='line_details',null=True, on_delete=models.CASCADE)
    Line_Activation_Date = models.DateTimeField(db_column='Line Activation Date')
    Line_Serial_Number = models.CharField(db_column='Line Serial Number',max_length=250)
    Line_Phone_Number = models.IntegerField(blank=True, null=True,db_column='Line Phone Number')

    class Meta:
        verbose_name_plural = "line_details"

class Calls_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='calls_details',null=True, on_delete=models.CASCADE)
    Call_Type = models.BooleanField(db_column='Call Type',max_length=250)
    Call_Duration = models.CharField(db_column='Call Duration',max_length=250)
    Call_Date = models.DateTimeField(db_column='Call Date')

    class Meta:
        verbose_name_plural = "calls_details"

class Device_details(models.Model):
    index = models.CharField(max_length=250, default='none', db_column='index')
    applicant = models.ForeignKey(Applicant_detail, related_name='device_details',null=True, on_delete=models.CASCADE)
    Device_Brand = models.CharField(db_column='Device Brand',max_length=250)
    Imei_Number = models.CharField(db_column='Imei Number',max_length=250)
    Sim_State = models.CharField(db_column='Sim State',max_length=250)
    Serial_Number = models.CharField(db_column='Serial Number',max_length=250)
    Country_ISO = models.CharField(db_column='Country_ISO',max_length=250)

    class Meta:
        verbose_name_plural = "device_details"




