import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pymongo import MongoClient
from  weightIv import ready_data
from sklearn import preprocessing
import joblib
if __name__ == '__main__':
    le = preprocessing.LabelEncoder()
    encoded_data = ready_data.apply(le.fit_transform)

    new_dataframe = encoded_data.iloc[0:500]


    X = pd.DataFrame(new_dataframe.iloc[:,:-1])
    y = pd.DataFrame(new_dataframe.iloc[:,-1])


    # Import train_test_split function
    from sklearn.model_selection import train_test_split

    # Split dataset into training set and test set
    X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25,random_state=0)
    #Import svm model
    from sklearn import svm

    #Create a svm Classifier
    clf = svm.SVC(kernel='linear') # Linear Kernel

    #Train the model using the training sets
    clf.fit(X_train, y_train)

    #save the model to disk
    filename = 'trained_svm_model.sav'
    joblib.dump(clf, filename)

    print(X_test)

    #Predict the response for test dataset
    y_pred = clf.predict(X_test)

    #Import scikit-learn metrics module for accuracy calculation
    from sklearn import metrics

    # Model Accuracy: how often is the classifier correct?
    print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
