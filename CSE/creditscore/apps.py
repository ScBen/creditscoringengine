from django.apps import AppConfig


class CreditscoreConfig(AppConfig):
    name = 'creditscore'
